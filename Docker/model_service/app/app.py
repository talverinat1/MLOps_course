from typing import List, Union
import os
from dotenv import load_dotenv
import yaml
import mlflow
import mlflow.spark
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from pyspark.ml.feature import VectorAssembler
from pyspark.sql import SparkSession

load_dotenv("./config/.env")

MLFLOW_TRACKING_URI = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(MLFLOW_TRACKING_URI)


spark = (
    SparkSession.builder.config("spark.driver.memory", "8g")
    .appName("App")
    .getOrCreate()
)

with open("./config/params.yaml", "r") as ymlfile:
    cfg = yaml.safe_load(ymlfile)

run_id = cfg["mlflow"]["run_id"]
logged_model = f"runs:/{run_id}/model"

# Load model
loaded_model = mlflow.spark.load_model(logged_model)

# Create a FastAPI app instance
app = FastAPI()


class PredictionInput(BaseModel):
    columns: List[str]
    index: List[int]
    data: List[List[Union[int, float]]]


# Define an API endpoint for making predictions
@app.post("/predict")
async def predict(payload: PredictionInput):  # Specify the expected input data type
    try:
        # Convert the Pydantic model to a dictionary
        data_dict = payload.dict()

        # Parse the JSON data
        data = spark.createDataFrame(data_dict["data"], data_dict["columns"])

        # Create a VectorAssembler to assemble features
        feature_columns = data.columns[:-1]
        assembler = VectorAssembler(inputCols=feature_columns, outputCol="features")
        data = assembler.transform(data)

        # Make predictions using your Decision Tree model
        predictions = loaded_model.transform(data).select("prediction")

        # Convert predictions to a list of dictionaries
        prediction_list = [
            {"prediction": row.prediction} for row in predictions.collect()
        ]

        # Return the predictions as a JSON response
        return {"predictions": prediction_list}
    except Exception as e:
        raise HTTPException(status_code=400, detail=str(e))


if __name__ == "__main__":
    import uvicorn

    # Serve the FastAPI app using uvicorn
    uvicorn.run(app, host="0.0.0.0", port=5002)
