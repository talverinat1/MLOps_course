import os
from typing import List

import click
import matplotlib.pyplot as plt
import mlflow
import mlflow.spark
import numpy as np
import pandas as pd
import yaml
from dotenv import load_dotenv
from pyspark.ml import Pipeline
from pyspark.ml.classification import DecisionTreeClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import VectorAssembler
from pyspark.sql import DataFrame, SparkSession
from pyspark.sql.functions import col
from pyspark.sql.types import DoubleType

load_dotenv("./config/.env")

MLFLOW_TRACKING_URI = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(MLFLOW_TRACKING_URI)


def class_probabilities(data: DataFrame) -> List[float]:
    """Calculate class probabilities in the pyspark DataFrame.

    :param DataFrame data: Input pyspark DataFrame
    :return List[float]: List of class probabilities
    """
    total = data.count()
    return (
        data.groupBy("Cover_Type")
        .count()
        .orderBy("Cover_Type")
        .select(col("count").cast(DoubleType()))
        .withColumn("count_proportion", col("count") / total)
        .select("count_proportion")
        .collect()
    )


def confusion_matrix_plot(pred_df: DataFrame, report_dir: str) -> None:
    """Generate and save a confusion matrix plot.

    :param DataFrame pred_df: DataFrame with predictions
    :param str report_dir: Directory to save the report.
    """
    confusion_matrix = (
        pred_df.groupBy("Cover_Type")
        .pivot("prediction", range(1, 8))
        .count()
        .na.fill(0.0)
        .orderBy("Cover_Type")
    )
    plot_filename = f"{report_dir}/figures/confusion_matrix_plot.png"

    # Convert the PySpark DataFrame to a Pandas DataFrame
    df_confusion = confusion_matrix.toPandas()
    df = df_confusion.set_index("Cover_Type")
    click.echo(df)

    # Plot confusion matrix
    plt.figure(figsize=(12, 10))
    cax = plt.matshow(df, cmap=plt.cm.Blues)
    plt.colorbar(cax)

    # Add labels
    plt.title("Confusion Matrix")
    plt.xlabel("Predicted")
    plt.ylabel("Actual")

    # Add values in each cell
    for i in range(df.shape[0]):
        for j in range(df.shape[1]):
            plt.text(j, i, str(df.iloc[i, j]), va="center", ha="center")

    # Add tick labels
    plt.xticks(np.arange(len(df.columns)), df.columns, rotation=45)
    plt.yticks(np.arange(len(df.index)), df.index)

    plt.savefig(plot_filename, format="png")


@click.command()
@click.argument("input_paths", type=click.Path(exists=True), nargs=2)
@click.argument("params_file", type=click.Path(exists=True))
@click.argument("report_dir", type=click.Path(exists=True))
def train(input_paths: List[str], params_file: str, report_dir: str):
    """Train the model and log params, metrics and artifacts in MLflow

    :param List[str] input_paths: train (for [0]) and test (for [1]) dataframes
    :param str params_file: file with params
    :param str report_dir: path to save reports
    """
    # Initialize a Spark session
    spark = SparkSession.builder.appName("TrainModel").getOrCreate()  # type: ignore

    # Read the CSV file into a DataFrame
    train_data = spark.read.csv(input_paths[0], header=True, inferSchema=True)
    test_data = spark.read.csv(input_paths[1], header=True, inferSchema=True)

    # Open file with params
    with open(params_file, "r") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    colnames = cfg["columns"]

    input_cols = colnames[:-1]
    vector_assembler = VectorAssembler(
        inputCols=input_cols, outputCol=cfg["classifier"]["featuresCol"]
    )

    assembled_train_data = vector_assembler.transform(train_data)
    click.echo(
        assembled_train_data.select(cfg["classifier"]["featuresCol"]).show(
            truncate=False
        )
    )

    classifier = DecisionTreeClassifier(
        seed=cfg["classifier"]["seed"],
        labelCol=cfg["classifier"]["labelCol"],
        featuresCol=cfg["classifier"]["featuresCol"],
        predictionCol=cfg["classifier"]["predictionCol"],
    )

    pipeline = Pipeline(stages=[vector_assembler, classifier])

    with mlflow.start_run(run_name="train-decision-tree"):
        mlflow.log_param("max_depth", classifier.getMaxDepth())

        pipeline_model = pipeline.fit(train_data)
        mlflow.spark.log_model(pipeline_model, "model")

        pred_df = pipeline_model.transform(test_data)
        evaluator = MulticlassClassificationEvaluator(
            labelCol=cfg["classifier"]["labelCol"],
            predictionCol=cfg["classifier"]["predictionCol"],
        )

        accuracy = evaluator.setMetricName("accuracy").evaluate(pred_df)
        f1 = evaluator.setMetricName("f1").evaluate(pred_df)
        mlflow.log_metrics({"accuracy": accuracy, "f1": f1})

        confusion_matrix_plot(pred_df, report_dir)

        tree_model = pipeline_model.stages[-1]

        debug_string = tree_model.toDebugString  # type: ignore
        debug_string_file = f"{report_dir}/tree_model_debug_string.txt"
        with open(debug_string_file, "w") as f:
            f.write(debug_string)

        feature_importance_df = pd.DataFrame(
            list(zip(vector_assembler.getInputCols(), tree_model.featureImportances)),
            columns=["feature", "importance"],
        ).sort_values(by="importance", ascending=False)

        feature_importance_df.to_csv(
            f"{report_dir}/feature-importance.csv", index=False
        )
        mlflow.log_artifact(f"{report_dir}/feature-importance.csv")

        train_prior_probabilities = class_probabilities(train_data)
        test_prior_probabilities = class_probabilities(test_data)

        train_prior_probabilities = [p[0] for p in train_prior_probabilities]
        test_prior_probabilities = [p[0] for p in test_prior_probabilities]

        sum_pairs = sum(
            [
                train_p * cv_p
                for train_p, cv_p in zip(
                    train_prior_probabilities, test_prior_probabilities
                )
            ]
        )

        mlflow.log_metric(
            "sum of products of pairs in training and test samples", sum_pairs
        )


if __name__ == "__main__":
    train()
