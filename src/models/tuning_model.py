import os
from pprint import pprint
from typing import List

import click
import mlflow
import mlflow.spark
import yaml
from dotenv import load_dotenv
from pyspark.ml import Pipeline
from pyspark.ml.classification import DecisionTreeClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.tuning import ParamGridBuilder, TrainValidationSplit
from pyspark.sql import SparkSession

load_dotenv("./config/.env")

MLFLOW_TRACKING_URI = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(MLFLOW_TRACKING_URI)


@click.command()
@click.argument("input_paths", type=click.Path(exists=True), nargs=2)
@click.argument("params_file", type=click.Path(exists=True))
def tuning_model(input_paths: List[str], params_file: str) -> None:
    spark = (
        SparkSession.builder.config("spark.driver.memory", "8g")
        .appName("TuningModel")
        .getOrCreate()
    )

    # Read the CSV file into a DataFrame
    train_data = spark.read.csv(input_paths[0], header=True, inferSchema=True)
    test_data = spark.read.csv(input_paths[1], header=True, inferSchema=True)

    # Open file with params
    with open(params_file, "r") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    colnames = cfg["columns"]

    input_cols = colnames[:-1]

    assembler = VectorAssembler(inputCols=input_cols, outputCol="featureVector")
    classifier = DecisionTreeClassifier(
        seed=cfg["classifier"]["seed"],
        labelCol=cfg["classifier"]["labelCol"],
        featuresCol=cfg["classifier"]["featuresCol"],
        predictionCol=cfg["classifier"]["predictionCol"],
    )

    pipeline = Pipeline(stages=[assembler, classifier])

    # Access the parameter values from the configuration
    impurity_values = cfg["paramGrid"]["classifier.impurity"]
    maxDepth_values = cfg["paramGrid"]["classifier.maxDepth"]
    maxBins_values = cfg["paramGrid"]["classifier.maxBins"]
    minInfoGain_values = cfg["paramGrid"]["classifier.minInfoGain"]

    # Build the paramGrid using the loaded values
    paramGrid = (
        ParamGridBuilder()
        .addGrid(classifier.impurity, impurity_values)
        .addGrid(classifier.maxDepth, maxDepth_values)
        .addGrid(classifier.maxBins, maxBins_values)
        .addGrid(classifier.minInfoGain, minInfoGain_values)
        .build()
    )

    multiclass_evaluator = (
        MulticlassClassificationEvaluator()
        .setLabelCol(cfg["multiclassEval"]["labelCol"])
        .setPredictionCol(cfg["multiclassEval"]["predictionCol"])
        .setMetricName(cfg["multiclassEval"]["metricName"])
    )

    validator = TrainValidationSplit(
        seed=cfg["validator"]["seed"],
        estimator=pipeline,
        evaluator=multiclass_evaluator,
        estimatorParamMaps=paramGrid,
        trainRatio=cfg["validator"]["trainRatio"],
    )

    with mlflow.start_run(run_name="tuning-decision-tree"):
        # Fit the validator on the training data
        validator_model = validator.fit(train_data)

        # Extract and log the best model
        best_model = validator_model.bestModel
        mlflow.spark.log_model(best_model, "best_model")

        # Log the parameters of the best model
        best_model_params = best_model.stages[1].extractParamMap()
        mlflow.log_params(best_model_params)

        # Log the validation metrics and parameter combinations
        metrics = validator_model.validationMetrics
        params = validator_model.getEstimatorParamMaps()
        metrics_and_params = list(zip(metrics, params))
        metrics_and_params.sort(key=lambda x: x[0], reverse=True)

        pprint(metrics_and_params)

        # Log the top metric value
        metrics.sort(reverse=True)
        top_metric = metrics[0]
        mlflow.log_metric("top_metric", top_metric)

        # Evaluate the best model on the test data and log the result
        test_accuracy = multiclass_evaluator.setMetricName("accuracy").evaluate(
            best_model.transform(test_data)
        )
        test_f1 = multiclass_evaluator.setMetricName("f1").evaluate(
            best_model.transform(test_data)
        )
        mlflow.log_metrics({"accuracy": test_accuracy, "f1": test_f1})

        # You can also log additional information as needed
        mlflow.log_param("train_ratio", validator.getTrainRatio())


if __name__ == "__main__":
    tuning_model()
