import yaml

# Define the colnames list
colnames = (
    [
        "Elevation",
        "Aspect",
        "Slope",
        "Horizontal_Distance_To_Hydrology",
        "Vertical_Distance_To_Hydrology",
        "Horizontal_Distance_To_Roadways",
        "Hillshade_9am",
        "Hillshade_Noon",
        "Hillshade_3pm",
        "Horizontal_Distance_To_Fire_Points",
    ]
    + [f"Wilderness_Area_{i}" for i in range(4)]
    + [f"Soil_Type_{i}" for i in range(40)]
    + ["Cover_Type"]
)

# Define the parameter dictionary
params = {"columns": colnames}

# Save the parameters to a YAML file
with open("config/params.yaml", "a") as ymlfile:
    yaml.dump(params, ymlfile)
