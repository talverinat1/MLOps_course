import gzip
import os
import shutil

import click
import wget


@click.command()
@click.argument("output_dir", type=click.Path(exists=True))
def download_data(output_dir: str):
    URL = "https://archive.ics.uci.edu/ml/machine-learning-databases/covtype/covtype.data.gz"

    filename = wget.download(URL)
    with gzip.open(filename, "rb") as f_in:
        output_file = os.path.join(output_dir, "covtype.csv")
        with open(output_file, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)

    # Delete the downloaded .gz file
    os.remove(filename)

    click.echo(f"Downloaded and extracted covtype data to {output_dir}/covtype.csv")


if __name__ == "__main__":
    download_data()
