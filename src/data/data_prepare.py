# Импортируйте библиотеку для работы с файлами
import json

import click
import yaml
from pyspark.sql import SparkSession
from pyspark.sql.functions import col
from pyspark.sql.types import DoubleType


@click.command()
@click.argument("input_data", type=click.Path(exists=True))
@click.argument("output_dir", type=click.Path(exists=True))
@click.argument("params_file", type=click.Path(exists=True))
def data_prepare(input_data: str, output_dir: str, params_file: str):
    spark = (
        SparkSession.builder.config("spark.driver.memory", "8g")
        .appName("DataPreparation")
        .getOrCreate()
    )

    # Load parameters from params.yml
    with open(params_file, "r") as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    # Get the train and test ratios from the configuration
    train_ratio = cfg["random_split"]["train_ratio"]
    test_ratio = cfg["random_split"]["test_ratio"]

    # Load DataFrame
    data_without_header = (
        spark.read.option("inferSchema", True).option("header", False).csv(input_data)
    )
    schema_json = data_without_header.schema.json()
    schema_dict = json.loads(schema_json)

    with open("./reports/schema.json", "w") as schema_file:
        json.dump(schema_dict, schema_file, indent=2)

    colnames = cfg["columns"]

    data = data_without_header.toDF(*colnames).withColumn(
        "Cover_Type", col("Cover_Type").cast(DoubleType())
    )

    # Select the first few rows (e.g., the first 5 rows)
    data_head = data.limit(5)

    # Save the selected rows as a JSON file in the 'reports' directory
    data_head.write.options(header="True", delimiter=",").format("csv").save(
        "reports/data_head.csv"
    )

    (train_data, test_data) = data.randomSplit([train_ratio, test_ratio])

    train_data.write.options(header="True", delimiter=",").format("csv").mode(
        "overwrite"
    ).save(f"{output_dir}/train/")
    test_data.write.options(header="True", delimiter=",").format("csv").mode(
        "overwrite"
    ).save(f"{output_dir}/test/")

    spark.stop()


if __name__ == "__main__":
    data_prepare()
