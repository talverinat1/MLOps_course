DecisionTreeClassificationModel: uid=DecisionTreeClassifier_497b0ca13f53, depth=5, numNodes=43, numClasses=8, numFeatures=54
  If (feature 0 <= 3052.5)
   If (feature 0 <= 2554.5)
    If (feature 10 <= 0.5)
     If (feature 0 <= 2453.5)
      If (feature 3 <= 15.0)
       Predict: 4.0
      Else (feature 3 > 15.0)
       Predict: 3.0
     Else (feature 0 > 2453.5)
      If (feature 17 <= 0.5)
       Predict: 2.0
      Else (feature 17 > 0.5)
       Predict: 3.0
    Else (feature 10 > 0.5)
     Predict: 2.0
   Else (feature 0 > 2554.5)
    If (feature 0 <= 2961.5)
     If (feature 15 <= 0.5)
      If (feature 17 <= 0.5)
       Predict: 2.0
      Else (feature 17 > 0.5)
       Predict: 3.0
     Else (feature 15 > 0.5)
      Predict: 3.0
    Else (feature 0 > 2961.5)
     If (feature 3 <= 191.0)
      If (feature 36 <= 0.5)
       Predict: 2.0
      Else (feature 36 > 0.5)
       Predict: 1.0
     Else (feature 3 > 191.0)
      Predict: 2.0
  Else (feature 0 > 3052.5)
   If (feature 0 <= 3313.5)
    If (feature 7 <= 239.5)
     If (feature 45 <= 0.5)
      Predict: 1.0
     Else (feature 45 > 0.5)
      If (feature 5 <= 3347.0)
       Predict: 2.0
      Else (feature 5 > 3347.0)
       Predict: 1.0
    Else (feature 7 > 239.5)
     If (feature 3 <= 340.5)
      Predict: 1.0
     Else (feature 3 > 340.5)
      Predict: 2.0
   Else (feature 0 > 3313.5)
    If (feature 12 <= 0.5)
     If (feature 3 <= 298.5)
      If (feature 6 <= 207.5)
       Predict: 1.0
      Else (feature 6 > 207.5)
       Predict: 7.0
     Else (feature 3 > 298.5)
      Predict: 1.0
    Else (feature 12 > 0.5)
     If (feature 45 <= 0.5)
      Predict: 7.0
     Else (feature 45 > 0.5)
      If (feature 5 <= 995.5)
       Predict: 7.0
      Else (feature 5 > 995.5)
       Predict: 1.0
