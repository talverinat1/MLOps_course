MLOps  Course project

[ODS](https://ods.ai/tracks/ml-in-production-spring-23)
==============================

Pyspark Decition Tree model for covtype.data

Project Organization
------------

    ├── config             <- Contain various configuration files
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── Docker             <- DockerFiles and docker-compose
    │
    ├── notebooks          <- example of pipeline, request test and mlflow service test
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features       <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │       │                 predictions
    │       ├── tuning_model.py
    │       └── train_model.py
   
==============================

MLFLow setup by 4 scenario [blog](https://blog.min.io/setting-up-a-development-machine-with-mlflow-and-minio/)

Covtype dataset [site](https://archive.ics.uci.edu/dataset/31/covertype)

==============================

Start project

On local computer create dir and add credention for minio

```bash
mkdir -p ./.aws/credentials
```
add this 

```
[default]
AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
```


Install libs
```bash
poetry install
```

Set up services that run by docker
```bash
docker-compose --env-file ../config/.env up -d --build
```
Setup dvc
```bash
poetry run dvc remote add -d minio s3://${S3_BUCKET} -f
poetry run dvc remote modify minio endpointurl http://${MINIO_HOST}:${MINIO_PORT_BACKEND}
poetry run dvc remote modify --local minio access_key_id ${AWS_ACCESS_KEY_ID}
poetry run dvc remote modify --local minio secret_access_key ${AWS_SECRET_ACCESS_KEY}
```
Run dvc stages 
```bash
poetry run dvc repro
```
